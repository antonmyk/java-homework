package main.java.Homework.Homework2;
import java.util.Scanner;


public class Planner {
    static private String[][] scedule = new String[7][2];
    public static void day_week(){
        scedule[0][0] = "Sunday";
        scedule[0][1] = "do home work";
        scedule[1][0] = "Monday";
        scedule[1][1] = "go to courses; watch a film";
        scedule[2][0] = "Tuesday";
        scedule[2][1] = "go to gym";
        scedule[3][0] = "Wednesday";
        scedule[3][1] = "repair car";
        scedule[4][0] = "Thursday";
        scedule[4][1] = "go to the marketplace";
        scedule[5][0] = "Friday";
        scedule[5][1] = "buy food";
        scedule[6][0] = "Saturday";
        scedule[6][1] = "free time";

        }
    static private String day;
    public static void planner_day(){
        Scanner plans = new Scanner(System.in);
        System.out.println("Please, input the day of the week:");
        day = plans.nextLine().toLowerCase().replaceAll("\\s+","");
        switch(day){
            case "monday":
                System.out.println("Your tasks for " + scedule[1][0] + ": " + scedule[1][1]);
                planner_day();
                break;
            case "tuesday":
                System.out.println("Your tasks for " + scedule[2][0] + ": " + scedule[2][1]);
                planner_day();
                break;
            case "wednesday":
                System.out.println("Your tasks for " + scedule[3][0] + ": " + scedule[3][1]);
                planner_day();
                break;
            case "thursday":
                System.out.println("Your tasks for " + scedule[4][0] + ": " + scedule[4][1]);
                planner_day();
                break;
            case "friday":
                System.out.println("Your tasks for " + scedule[5][0] + ": " + scedule[5][1]);
                planner_day();
                break;
            case "saturday":
                System.out.println("Your tasks for " + scedule[6][0] + ": " + scedule[6][1]);
                planner_day();
                break;
            case "sunday":
                System.out.println("Your tasks for " + scedule[0][0] + ": " + scedule[0][1]);
                planner_day();
                break;
            case "exit":
                System.out.println("Thank you for use our service");
                break;
            default: //Default case
                System.out.println("Sorry, I don't understand you, please try again.");
                planner_day();
                break;
        }
    }
    public static void main(String[] args) {
        day_week();
        planner_day();
    }
}
