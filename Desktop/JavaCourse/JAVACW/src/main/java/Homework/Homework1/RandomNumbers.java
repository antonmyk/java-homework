package main.java.Homework.Homework1;
import java.util.Scanner;
import java.util.Random;

public class RandomNumbers {
    static private String name = "";
    public static void getName(){
        Scanner sd = new Scanner(System.in);
        System.out.print("Input name: ");
        name = sd.nextLine();
        System.out.print("\n" + name);
    }
    static private int arr = 0;
    static private int arrNum = 0;
    public static void getRandom() {
        Random rd = new Random();
        arr = rd.nextInt(100);
        System.out.print("\nLet the game begin!");
    }
    public static void enter_number(){
        Scanner in = new Scanner(System.in);
        System.out.print("\nInput your number from 1 to 100: ");
        if (in.hasNextInt()) {
            arrNum = in.nextInt();
        } else {
            System.out.println("Вы ввели не целое число");
        }
    }
    public static void check_random(){
        for(int i = 0; i < 100; i++ ) {
            if (arr == arrNum) {
               System.out.print("\nCongratulations, " + name);
               return;
            } else if (arrNum < arr) {
                System.out.print("Your number is too small. Please, try again.");
                enter_number();
            } else if (arrNum > arr) {
                System.out.print("Your number is too big. Please, try again.");
                enter_number();
            } else {
                System.out.print("Please, try again.");
                enter_number();
            }
        }
    }
    public static void main(String[] args) {
        getName();
        getRandom();
        enter_number();
        check_random();
    }
}